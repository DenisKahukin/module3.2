﻿using System;
using System.Collections.Generic;

namespace Module3_2
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if(int.TryParse(input, out result))
            {
                if (result >= 0) return true;
                if (result == -1) return false;
            }
            ShowMessage("The input value not correctly, please input other number or input '-1' to exit");
            return TryParseNaturalNumber(input, out result);
        }
        private void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
        private void ShowMessage(int[] array)
        {
            for (int i = 0; i < array.Length; i++) {
                Console.Write(array[i] + " ");
        } 
        }

        public int[] GetFibonacciSequence(int n)
        {
            if (n <= 0) return new int[] { };
            
            int[] array = new int[n];
            int firstNumber = 0;
            int secondNumber = 1;
            for (int i = 0; i < array.Length; i++) 
            {
                array[i] = firstNumber;
                firstNumber = secondNumber;
                secondNumber += array[i];
            }
            ShowMessage(array);
            return array;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            int reversNumber = 0;
            while(sourceNumber != 0)
            {
                reversNumber = reversNumber * 10 + sourceNumber % 10;
                sourceNumber /= 10; 
            }
            return reversNumber;
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            if (size <= 0) return new int[] { };
            Random rand = new Random();
            int[] array = new int[size];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.Next(int.MinValue, int.MaxValue);
            }
            return array;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            if (source.Length <= 0) return source;
            for (int i = 0; i < source.Length; i++)
            {
                source[i] *= -1;
            }
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            if (size <= 0) return new int[] { };
            Random rand = new Random();
            int[] array = new int[size];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.Next(int.MinValue, int.MaxValue);
            }
            return array;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            if (source.Length <= 0) return new List<int>();
            List<int> listForElement = new List<int>();
            try
            {
                for (int i = 0; i < source.Length; i++)
                {
                    if (source[i] < source[i + 1])
                    {
                        listForElement.Add(source[i + 1]);
                    }
                }
            }
            catch
            {
                return listForElement;
            }
            return listForElement;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            if (size <= 0) return new int[,] { };
            int[,] array = new int[size, size];
            int currentChar, padding;
            for (currentChar = 1, padding = 1; padding <= size / 2; padding++)
            {
                array = FillTheTopOfArray(array, padding, size, ref currentChar);
            }
            return array;
        }

        private int[,] FillTheTopOfArray(int[,] array, int padding, int size, ref int currentChar)
        {
            int index;
            for (index = padding - 1; index < size - padding + 1; index++, currentChar++)
            {
                array[padding - 1, index] = currentChar;
            }
            for (index = padding; index < size - padding + 1; index++, currentChar++)
            {
                array[index, size - padding] = currentChar;
            }
            array = FiilTheBottomOfArray(array, padding, size, ref currentChar);
            return array;
        }


        private int[,] FiilTheBottomOfArray(int[,] array,  int padding,  int size, ref int currentChar)
        {
            int index;
            for (index = size - padding - 1; index >= padding - 1; --index)
            {
                array[size - padding, index] = currentChar++;
            }
            for (index = size - padding - 1; index >= padding; index--)
            {
                array[index, padding - 1] = currentChar++;
            }

            return array;
        }
    }
}
